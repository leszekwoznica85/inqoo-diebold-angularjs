// jQuery('button').css('color') // get
// jQuery('button').css('color', 'red') //set




const app = angular.module("myApp", ['tasks']);


// Global scope
/*
app.run(function ($rootScope) {

});
*/

// local scope
app.controller("ApptCtrl", ($scope) => {
  $scope.title = "MyApp Leszek";
  $scope.user = { name: "Guest" };
  $scope.isLoggedIn = false;

  $scope.login = () => {
    $scope.user.name = "Admin";
    $scope.isLoggedIn = true;
  };

  $scope.logout = () => {
    $scope.user.name = "Guest";
    $scope.isLoggedIn = false;
  };
});

// Start !

// <html ng-app="myApp" // auto-boostrap
angular.bootstrap(document, ["myApp"]); // manual boostrap
