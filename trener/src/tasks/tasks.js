
const tasks = angular.module('tasks', [])


tasks.controller('TasksCtrl', ($scope) => {

  $scope.mode = 'show'

  $scope.tasks = [{
    id: "123",
    name: "Task 123",
    completed: true
  }, {
    id: "234",
    name: "Task 234",
    completed: false
  }, {
    id: "345",
    name: "Task 345",
    completed: true
  }];

  $scope.draft = {}

  $scope.selectedTask = {
    id: "123",
    name: "Task 123",
    completed: true
  }

  $scope.edit = () => {
    $scope.mode = 'edit'
    // $scope.draft = $scope.selectedTask
    $scope.draft = { ...$scope.selectedTask }
  }

  $scope.cancel = () => {
    $scope.mode = 'show'
  }

  $scope.save = () => {
    $scope.mode = 'show'
    $scope.selectedTask = { ...$scope.draft }
    $scope.tasks = $scope.tasks.map(
      task => task.id == $scope.draft.id ? $scope.draft : task
    )
  }

  $scope.select = (item) => {
    $scope.selectedTask = item
  }
})