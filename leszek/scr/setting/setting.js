const settings = angular
  .module("settings", [])
  .controller("SettingsPageCtrl", ($scope) => {
    const vm = ($scope.settingsPage = {});
    vm.test = "hello";
    console.log(vm.test);
  });
angular.module("settings").directive("appHightlight", function (PAGES) {
  return {
    restrict: "EACM", //MACE
    link($scope, $element, attrs, ctrl) {
      console.log("Hello", $element);
      $element
        .css({ color: "red" })
        .html("<p>placki</p>")
        .on("click", event => {
            $element.find("p")[0].classList.toggle("text-success");
          }
          //$element(event.currentTarget).find("p").css("color", "blue")
        );
    },
  };
});

angular.module('settings').directive('appAlert', function () {
  return {
      scope:{messageTxt:'@message',  // szuka message w html i przypisuje ją do zmiennej message text
                    type: '@', //szuka tego samego w html czyli type="danger" type="info" type="success"
                    onDismiss:'&'}, //szuka tego sameg od-dismiss w html i sam robi z tego funkcję

    
    template: /* html */`<div class="alert alert-{{type}}" ng-if="$ctrl.open">
    <span class="close float-end" ng-click="$ctrl.close()" >&times;</span>
      <div>{{messageTxt}}<div>
    </div>`,
    controller($scope){
      const vm = $scope.$ctrl = {}
      vm.open = true;
      vm.close = () => {vm.open = false; $scope.onDismiss()}
    }
  }
})
