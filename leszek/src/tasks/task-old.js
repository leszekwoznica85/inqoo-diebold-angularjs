
const task = angular.module('tasks', [])

angular.module('tasks')
  .directive('tasksPage', function () {
    return {
      scope: {},
      template:/* html */`<div>tasksPage {{$ctrl.title}}
        <div class="row">
        <div class="col"><tasks-list></tasks-list></div>
        <div class="col"><task-details></task-details></div>
        </div>
      </div>`,
      controller($scope) {
        const vm = $scope.$ctrl= {}
        vm.title = 'Zadania'
        vm.test = 'Zadania'
        vm.tasks = [{ name: 'task1' }, { name: 'task1' }, { name: 'task1' }]
      }
    }
  })
  .directive('tasksList', function () {
    return {
      scope: {},
      template:/* html */`<div>tasksList {{$ctrl.title}}</div>
                         <div ng-repeat="task in $ctrl.tasks">
                          {{task.name}}
                         </div>        `
    }
  })
  .directive('tasksDetails', function () {
    return {
      scope: {},
      template:/* html */`<div>tasksDetails</div>`
    }
  })




//const task = angular.module("tasks", []);
task.controller("TaskCtrl", ($scope) => {
  $scope.tasks = [
    {
      id: "123",
      name: "Task 123",
      completed: true,
    },
    {
      id: "234",
      name: "Task 234",
      completed: false,
    },
    {
      id: "345",
      name: "Task 345",
      completed: true,
    },
  ];

  $scope.draft = {};

  $scope.taskName = "";

  $scope.filterTask = [...$scope.tasks];
  $scope.mode = "show";

  $scope.numberDoneTask = 0;
  $scope.numberNotDoneTask = 0;

  $scope.echo = () => {
    console.log($scope.mode);
    $scope.mode = "edit";
    $scope.nowa = { ...$scope.draft };
  };
  $scope.clear = () => {
    $scope.nowa = {};
  };
  $scope.save = () => {
    console.log($scope.draft);
    $scope.draft = { ...$scope.nowa };
    let numberOfIndex = $scope.tasks.findIndex(
      (task) => task.id === $scope.draft.id
    );
    $scope.tasks.splice(numberOfIndex, 1, $scope.draft);
    $scope.searchTask();
    console.log($scope.draft);
    // $scope.taskList()
  };
  $scope.hidden = () => {
    $scope.mode = "";
  };
  $scope.selected = (id) => {
    $scope.draft = $scope.tasks.find((task) => task.id === id);

    //console.log($scope.draft)
  };
  $scope.deleteElement = (id) => {
    console.log(id);
    console.log($scope.tasks.splice(id, 1));
    $scope.searchTask();

    //$scope.task.tasks.splice(0,id)
  };
  $scope.newTask = () => {
    $scope.mode = "edit";
  };
  $scope.saveNewTask = () => {
    // console.log($scope.nowa)

    $scope.tasks.push({
      id: "523",
      name: $scope.nowa.name,
      completed: false,
    });
    console.log($scope.tasks);
    //$scope.taskList()
    $scope.searchTask();
  };
  $scope.searchTask = () => {
    //console.log($scope.taskName)
    $scope.filterTask = $scope.tasks.temp((task) =>
      task.name.toLowerCase().includes($scope.taskName.toLowerCase())
    );
    console.log($scope.filterTask);
    $scope.taskList();
    //$scope.tasks = {...$scope.draft}
  };
  $scope.taskList = () => {
    $scope.numberDoneTask = 0;
    $scope.numberNotDoneTask = 0;
    //console.log($scope.tasks);
    $scope.tasks.forEach((element) => {
      element.completed ? $scope.numberDoneTask++ : $scope.numberNotDoneTask++;
    });
  };
  $scope.taskList();
});
