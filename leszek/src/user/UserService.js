angular.module("users.service", []).service("UsersService", function ($http) {

    this.getUsers = () => {
      return this._users;
    };
    this.updateUser = (draft) => {
      console.log(draft.id);
      return $http.put("http://localhost:3000/users/"+ draft.id,draft)
    };
    this.getUserById = (id) => {
      return this._users.find((u) => u.id == id);
    };
    this.fetchUsers = () => {
      return $http.get("http://localhost:3000/users").then((res) => {
        return (this._users = res.data);
      });
    };
    this.addNewUser = (draft) => {
      //console.log(draft);
      return $http.post("http://localhost:3000/users/", draft)
    }
    this.deleteNewUser = (id) => {
      //console.log(draft);
      return $http.delete("http://localhost:3000/users/"+ id,)
    }
  });