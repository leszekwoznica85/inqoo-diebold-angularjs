const usersEditForm = angular.module('usersEditForm', [])

angular.module('usersEditForm').controller('UsersEditFormCtrl', ($scope, UsersService) => {
const vm = $scope.userEditForm ={}
     console.log('Hello UsersEditFormCtrl', UsersService)
     vm.save = (draft) => {
        // console.log(draft);
        vm.selected = UsersService.updateUser(draft)
        vm.select(draft.id)
        vm.users = UsersService.getUsers()
         console.log(vm.users);
     }

})