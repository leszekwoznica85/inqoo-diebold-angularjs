const users = angular.module("users", ["users.service"]);

angular.module("users").controller("UsersPageCtrl", ($scope, UsersService,$timeout) => {
  const vm = ($scope.usersPage = {});
  //console.log('Hello UsersPageCtrl', UsersService)
  //const vm = $scope.UsersPageCtrl

  vm.selected = null;
  vm.users = UsersService.getUsers();
  //vm.users = vm.refresh
  //console.log(vm.users);
  vm.edit = () => {
    vm.mode = "edit";
  };
  vm.creatNew = () => {
    vm.mode = "edit";
    vm.formMode = "new";
    vm.selected = true;
    vm.selectedUserTemp = {};
    console.log(vm.formMode);
  };

  vm.select = (id) => {
    vm.selectedUser = UsersService.getUserById(id);
    vm.selectedUserTemp = { ...vm.selectedUser };
    vm.selected = true;
    vm.mode = "show";
    vm.formMode = "edit";
    console.log(vm.formMode);

    //console.log(id)
    //console.log($scope.selected)
  };

  vm.save = (draft) => {
    //console.log(draft);
    if (vm.form.$invalid) {
      vm.showMessage("Form has errors");
      return;
    }

    if (draft.id) {
      UsersService.updateUser(draft);
    } else {
      draft.id = vm.users.length + 1;
      UsersService.addNewUser(draft);
    }
    vm.refresh();
  };
  vm.refresh = () => {
    UsersService.fetchUsers().then((data) => {
      vm.users = data;
    });
  };
  vm.refresh();
  vm.deleteUser = (id) => {
    //console.log(id)
    UsersService.deleteNewUser(id);
    vm.refresh();
  };

  vm.showMessage = (msg) => {
    
    vm.message = msg;
    console.log(vm.message);
    $timeout(() => {
      vm.message = "";
    }, 2000);
    
  };
});
