// jQuery('button').css('color') // get
// jQuery('button').css('color', 'red') //set
const app = angular
  .module("myApp", ["tasks", "users.service", "users", "settings", "ui.router"])
  .constant("PAGES", [
    { name: "users", label: "Users", tpl: "./views/users-view.tpl.html" },
    { name: "tasks", label: "Tasks", tpl: "./views/tasks-view.tpl.html" },
    {
      name: "settings",
      label: "Settings",
      tpl: "./views/settings-view.tpl.html",
    },
  ]);
// Global scope
/*
app.run(function ($rootScope) {

});
*/
app.config(function (PAGES) {
  // console.log(PAGES)
});
// local scope
app.controller("ApptCtrl", ($scope, PAGES) => {
  $scope.title = "MyApp Leszek";
  $scope.user = { name: "Guest" };
  $scope.isLoggedIn = false;

  $scope.login = () => {
    $scope.user.name = "Admin";
    $scope.isLoggedIn = true;
  };

  $scope.logout = () => {
    $scope.user.name = "Guest";
    $scope.isLoggedIn = false;
  };
  $scope.showCard = "";

  $scope.navigationShow = (cardToShow) => {
    $scope.showCard = cardToShow;
  };
  $scope.pages = PAGES;
/*
  $scope.currentPage = $scope.pages[4];

  $scope.goToPage = (pageName) => {
    $scope.currentPage = $scope.pages.find((p) => p.name === pageName);
    // console.log($scope.currentPage)
 
  };
     */
});
app.config(function ($stateProvider) {
  $stateProvider
    .state({
      name: "users",
      url: "/users",
      //template: "<h1>users</h1>",
      templateUrl: "/views/users-view.tpl.html"
    })
    .state({
      name: "tasks",
      url: "/tasks",
      template: "<tasks-page></tasks-page>",
    })
    .state({
      name: "settings",
      url: "/settings",
      templateUrl: "/views/settings-view.tpl.html",
    });
});

// Start !

// <html ng-app="myApp" // auto-boostrap
angular.bootstrap(document, ["myApp"]); // manual boostrap
