var model = {
    user: "Leszek",
    items: [{ name: "Asp.NET MVC", attended: false },
    { name: "angularjs", attended: true },
    { name: "mysql", attended: true },
    { name: "Unity", attended: true },
    { name: "WPF", attended: false }]
    };
 
var courseListApp = angular.module("courseListApp", []);

courseListApp.controller("courseController", function ($scope) {
    $scope.courses = model;
});

angular.bootstrap(document, ["courseListApp"]); // manual boostrap