describe('Testowy modul', () => {
  it('adds numbers 1 and 3', () => {
    /* function adder(x,y) */

    expect(adder(-1, 3)).toEqual(2)
    expect(adder(1, 3)).toEqual(4)
  })
})