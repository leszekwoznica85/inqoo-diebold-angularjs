const users = angular.module("users", []);
console.log("modul users");
users.component("usersPage", {
  bindings: {},
  template: /*HTML*/ `

  <div>
    <h1>{{$ctrl.title}}</h1>
      <div class="row">
        <div class="col"> <task-list
         users='$ctrl.users'
         on-select="$ctrl.selectUser($event)"></task-list> </div>
        <div class="col" ng-if="$ctrl.mode == 'details'" >
        <user-details user="$ctrl.selected" on-click="$ctrl.changeView()"></user-details>
        </div>
        <div class="col" ng-if="$ctrl.mode == 'edit'">
        <user-form usertemp="$ctrl.selected"></user-form>
        </div>
        </div>
      </div>
  </div>`,
  controller: function (UsersService, $timeout) {
    this.title = "Strona użytkowników";
    this.users = [];
    this.mode = null;
    $timeout(() => {
      UsersService.fetchUsers().then((data) => {
        this.users = data;
        this.selected = this.users[1];
      });
    }, 1000);
    this.selectUser = (temp) => {
      console.log(temp);
      this.selected = temp;
      this.mode = "details";
    };
    this.changeView = () => {
     console.log('changeView')
      this.mode = "edit";
    };
  },
});

users.component("taskList", {
  bindings: {
    users: "<",
    onSelect: "&",
  },
  template: /*HTML */ `
    <div>
      <h2>Lista</h2>
      <div class="list-group-item" ng-repeat='user in $ctrl.users' ng-click= "$ctrl.listSelectUser(user)">
      {{user.id}} {{user.name}}
      </div>
    </div>
  `,
  controller() {
    this.listSelectUser = function (user) {
      // console.log('listSelectUser', user);
      this.onSelect({ $event: user });
    };
  },
});

users.component("userDetails", {
  bindings: {
    user: "<",
    onClick: "&",
  },
  template: /*HTML */ `<div>
    <h3>User details<h3>
    <dl><dt>User name =</dt><dd>{{$ctrl.user}} </dd></dl>
  </div>
  <button ng-click="$ctrl.changeView()">Edit</button>
  `,

  controller() {
    this.changeView = () => {
      console.log("edit"), this.onClick();
    };
  },
});
users.component("userForm", {
  bindings: {
    usertemp: "<",
  },
  template: /*HTML*/ `
  <div>
    <h3>user form edit</h3>
    <input type="text" ng-model="$ctrl.draft.name">
  </div>
  `,
  controller() {
    this.$onChanges = () => {
      this.draft = { ...this.usertemp };
    };
  },
});
users.service("UsersService", UsersService);

function UsersService($q, $http) {
  this.fetchUsers = () => {
    return $q.resolve([
      //promiss
      { id: "123", name: "Bardzo dluuuuga nazwao uzytkownika 123" },
      { id: "234", name: "User 234" },
      { id: "345", name: "User 345" },
    ]);
  };
}
